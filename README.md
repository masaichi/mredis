# mredis

#### 介绍
封装redis，主要是对goredis的简单封装

#### 软件架构
1. 依赖 github.com/go-redis/redis/v8
2. 使用sync.Pool进行连接管理


#### 安装教程

1.  go get -u gitee.com/masaichi/mredis

#### 使用说明

1.  创建mredis示例
```
redisService := NewMRedis()
```
2. 设置连接参数
```
redisService.SetConn(viper.GetString("host"), viper.GetInt("port"), viper.GetString("password"), viper.GetInt("db"))
```
3. 使用
```
        //设置缓存
        dataStr, _ := json.Marshal(data)
	redisService.SetCache("name123", string(dataStr))
        //获取缓存
        res := redisService.GetCache("name123")
        //加锁 时间默认30秒
        if redisService.Lock(lockKey) {
		log.Println("锁成功")
	}
        //解锁
        redisService.Unlock(lockKey)
        //删除缓存
        redisService.Del(key)
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
