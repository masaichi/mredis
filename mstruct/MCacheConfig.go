package mstruct

type MCacheConfig struct {
	Host     string
	Port     int
	Password string
	DB       int
}
