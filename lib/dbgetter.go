package lib

import (
	"gitee.com/masaichi/mredis/goredis"
	"log"
)

func NewsDBGetter(id string) goredis.DBGettFunc {
	return func() interface{} {
		log.Println("get from db")
		newsModel := NewNewsModel()
		Gorm.Table("news").Where("id=?", id).Find(newsModel)
		return newsModel
	}
}
