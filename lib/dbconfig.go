package lib

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)

var Gorm *gorm.DB

func init() {
	Gorm = gormDB()
}

func gormDB() *gorm.DB {
	dsh := "MASAICHI:85168908@tcp(47.122.0.10:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsh), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	mysqlDB, err := db.DB()
	if err != nil {
		log.Fatal(err)
	}
	mysqlDB.SetMaxIdleConns(5)
	mysqlDB.SetMaxOpenConns(10)
	return db
}
