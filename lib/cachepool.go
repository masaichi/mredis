package lib

import (
	"gitee.com/masaichi/mredis/goredis"
	"gitee.com/masaichi/mredis/mstruct"
	"sync"
	"time"
)

var NewsCachePool *sync.Pool

type McachePool struct {
	mstruct.MCacheConfig
}

func NewMCachePool(config mstruct.MCacheConfig) *McachePool {
	return &McachePool{MCacheConfig: config}
}

//设置连接信息
func (this *McachePool) SetConn() {
	NewsCachePool = &sync.Pool{
		New: func() interface{} {
			return goredis.NewSimpleCache(goredis.NewStringOperation(this.MCacheConfig), time.Second*15, goredis.Serilizer_JSON)
		},
	}
}

func NewMCache() *goredis.SimpleCache {
	return NewsCachePool.Get().(*goredis.SimpleCache)
}

//释放连接
func ReleaseMCache(cache *goredis.SimpleCache) {
	NewsCachePool.Put(cache)
}
