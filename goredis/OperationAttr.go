package goredis

import (
	"fmt"
	"time"
)

const ATTR_EXPIRE = "expire"

type OperationAttr struct {
	Name  string
	Value interface{}
}

type OperationAttrs []*OperationAttr

//查找是否有该属性
func (this OperationAttrs) Find(name string) *InterfaceResult {
	for _, attr := range this {
		if attr.Name == name {
			return NewInterfaceResult(attr.Value, nil)
		}
	}
	return NewInterfaceResult(nil, fmt.Errorf("operation found error %s", name))
}

//设置过期时间
func WithExpire(t time.Duration) *OperationAttr {
	return &OperationAttr{Name: ATTR_EXPIRE, Value: t}
}
