package goredis

type StringResult struct {
	Result string
	Err    error
}

func NewStringResult(result string, err error) *StringResult {
	return &StringResult{Result: result, Err: err}
}

func (this *StringResult) Unwrap() string {
	if this.Err != nil {
		panic(this.Err)
	}
	return this.Result
}

func (this *StringResult) UnwrapDefault(def string) string {
	if this.Err != nil {
		return def
	}
	return this.Result
}

//如果获取不到缓存，则执行默认方法
func (this *StringResult) UnwrapElse(f func() string) interface{} {
	if this.Err != nil {
		return f()
	}
	return this.Result
}
