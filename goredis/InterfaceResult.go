package goredis

//为什么要这个类，是因为redis方法的返回值不一样，有些是string,error, 有些是 bool, error，这个结构体兼容到了,对返回结果进行统一封装
type InterfaceResult struct {
	Result interface{}
	Err    error
}

func NewInterfaceResult(result interface{}, err error) *InterfaceResult {
	return &InterfaceResult{Result: result, Err: err}
}

func (this *InterfaceResult) UnWrap() interface{} {
	if this.Err != nil {
		panic(this.Err)
	}
	return this.Result
}

func (this *InterfaceResult) UnWrapDefault(def interface{}) interface{} {
	if this.Err != nil {
		this.Result = def
	}
	return this.Result
}
