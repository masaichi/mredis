package mredis

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"log"
)

func main() {
	//获取缓存
	//ca := lib.NewsCache()
	//defer lib.ReleaseNewsCache(ca)
	////2. 获取参数，设置dbgetter
	//newsID := "2"
	//ca.DBGeter = lib.NewsDBGetter(newsID)
	//log.Fatal(ca.GetCache("news" + newsID))
	testnew()
}

func testnew() {
	//1. 设置缓存连接
	viper.SetConfigName("config")
	viper.SetConfigType("env")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}

	//设置连接
	redisService := NewMRedis()
	redisService.SetConn(viper.GetString("host"), viper.GetInt("port"), viper.GetString("password"), viper.GetInt("db"))
	////2. 进行获取
	data := map[string]interface{}{
		"Id":   1,
		"Name": "123",
	}
	lockKey := "lock"
	if !redisService.Lock(lockKey) {
		log.Println("锁失败")
	}
	redisService.Unlock(lockKey) //解锁
	if redisService.Lock(lockKey) {
		log.Println("锁成功")
	}
	dataStr, _ := json.Marshal(data)
	redisService.SetCache("name123", string(dataStr))
	res := redisService.GetCache("name123")
	fmt.Println(res)
}
