package mredis

import (
	"gitee.com/masaichi/mredis/lib"
	"gitee.com/masaichi/mredis/mstruct"
	"time"
)

type MRedis struct {
	mstruct.MCacheConfig
	LockExpire time.Duration
}

//设置连接
func (this *MRedis) SetConn(host string, port int, pwd string, db int) {
	mconfig := mstruct.MCacheConfig{
		Host:     host,
		Port:     port,
		Password: pwd,
		DB:       db,
	}
	this.MCacheConfig = mconfig
	pool := lib.NewMCachePool(mconfig)
	pool.SetConn()
}

//实例化一个操作对象
func NewMRedis() *MRedis {
	return &MRedis{LockExpire: time.Second * 30} //设置默认锁时间为30s
}

//获取缓存
func (this *MRedis) GetCache(key string) interface{} {
	return lib.NewMCache().GetCache(key)
}

//设置缓存
func (this *MRedis) SetCache(key string, val interface{}) {
	lib.NewMCache().SetCache(key, val)
}

//锁
func (this *MRedis) Lock(key string) bool {
	return lib.NewMCache().Lock(key, this.LockExpire)
}

//解锁
func (this *MRedis) Unlock(key string) {
	lib.NewMCache().Unlock(key)
}

//删除缓存
func (this *MRedis) Del(key string) {
	lib.NewMCache().Unlock(key)
}
